v0.8.0 2013-09-24 Lausanne
--------------------------

First release.  
Part of the work was sponsored by Citrix Systems R&D and OCaml Labs.
